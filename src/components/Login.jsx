import React from "react";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import { withStyles } from "@material-ui/styles";
import styles from "./Login.styles";

const Login = ({ classes, sendSignal, rooms, show, onUserName }) => {
  const [values, setValues] = React.useState({
    username: ""
  });
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  function handleClose() {
    setAnchorEl(null);
  }

  const handleRoomSelect = name => event => {
    console.info("Selected room: " + name);
    sendSignal({ type: "join", room: name });
    handleClose();
  };

  const handleSignIn = event => {
    event.preventDefault();
    console.info("username: " + values.username);
    sendSignal({ type: "login", name: values.username });
    setAnchorEl(event.currentTarget);
    onUserName(values.username);
  };

  return show ? (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            value={values.username}
            onChange={handleChange("username")}
            required
            variant="outlined"
            margin="normal"
            fullWidth
            id="username"
            label="User Name"
            name="username"
            autoFocus
          />
          <Button
            disabled={Boolean(anchorEl)}
            onClick={handleSignIn}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
        </form>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl) && Boolean(rooms) && rooms.length > 0}
          onClose={handleClose}
        >
          {rooms &&
            rooms.map(name => (
              <MenuItem key={name} onClick={handleRoomSelect(name)}>
                {name}
              </MenuItem>
            ))}
        </Menu>
      </div>
    </Container>
  ) : null;
};

export default withStyles(styles)(Login);
