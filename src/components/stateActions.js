export const removeStream = name => state => {
  const streams = { ...state.streams };
  delete streams[name];
  return { streams };
};

export const addStream = (name, stream) => state => {
  const streams = { ...state.streams, [name]: stream };
  return { streams };
};
