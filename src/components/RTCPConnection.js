import React, { Component } from "react";
import { PEER_CONFIGURATION, DATA_TRANSFER_OPTIONS } from "./config";

const DEFAULT_OFFER_OPTIONS = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: false
};

const VIDEO_OFFER_OPTIONS = {
  offerToReceiveAudio: false,
  offerToReceiveVideo: true
};

// TODO: make it a class and abstract away yourConections as a hook
class RTCPConnection extends Component {
  yourConnections = {}; // TODO: connections should be kept in GLOBAL state, as they needed while component is dismounted
  yourDataChannels = { incoming: {}, outcoming: {} }; // TODO: connections should be kept in GLOBAL state, as they needed while component is dismounted
  senders = {};

  componentDidMount() {
    this.props.onSubscribe({
      transport: this.broadcastData,
      startStreaming: this.startStreaming,
      stopStreaming: this.stopStreaming
    });
  }

  sendSignal = () =>
    console.log("OOPS. We didnt get sendSignal method from WebSocket");

  handleWebSocketReady = sendSignalTransport =>
    (this.sendSignal = sendSignalTransport);

  onNewUser = data => {
    console.info("New user joined the room: " + data.name);
    this.startPeerConnection(data.name);
  };

  onOffer = async (offer, from) => {
    const connection =
      this.getConnection(from) || this.setupPeerConnection(from);
    try {
      const answer = await this.createAnswer(connection, offer); // TODO: add error handlers
      this.sendSignal({ type: "answer", answer, name: from });
    } catch (e) {
      console.warn("Something went wrong while answering onOffer: ", e);
      this.deleteConnection(from);
    }
  };

  onAnswer = async (answer, from) => {
    const connection = this.getConnection(from);
    await connection.setRemoteDescription(new RTCSessionDescription(answer));
  };
  // TODO: add error handlers
  onICECandidate = async (candidate, from) => {
    const connection = this.getConnection(from);
    if (!connection)
      throw new Error(
        "No connection while server send us ICECandidate from: " + from
      );
    await connection.addIceCandidate(new RTCIceCandidate(candidate));
  };

  onHangup = ({ name }) => {
    const connection = this.getConnection(name);
    if (connection) {
      connection.close();
      connection.onicecandidate = null;
      connection.onaddstream = null;
      this.props.onHangup(name);
    }
  };

  onHangupVideo = ({ name }) => {
    const connection = this.getConnection(name);
    if (connection) {
      // this.removeLocalVideoStream(connection, name);
      this.props.onHangupVideo(name);
    } else {
      console.warn(`No connection found. Can't hangup VIDEO of user: ${name}`);
    }
  };

  startPeerConnection = async destinationUserName => {
    const connection = this.setupPeerConnection(destinationUserName);
    try {
      const offer = await this.createOffer(connection);
      this.sendSignal({ type: "offer", offer, name: destinationUserName }); // TODO: abstract all messages
    } catch (e) {
      console.warn("Something went wrong while creating offer: ", e);
    }
  };
  // TODO: abstract it away as a class
  addNewConnection = (name, connection) =>
    (this.yourConnections[name] = connection);
  getConnection = name => this.yourConnections[name];
  deleteConnection = name => {
    delete this.yourConnections[name];
  };
  connectionExist(name) {
    return this.yourConnections.hasOwnProperty(name);
  }
  get connections() {
    return Object.values(this.yourConnections);
  }
  get users() {
    return Object.keys(this.yourConnections);
  }

  // TODO: abstract it away as a class
  addNewDataChannel = (name, connection, group) => {
    console.info("Adding new " + group + " channel to repository: " + name);
    this.yourDataChannels[group][name] = connection;
    console.info(this.yourDataChannels);
  };
  getDataChannel = (name, group) => this.yourDataChannels[group][name];
  deleteDataChannel = name => {
    delete this.yourDataChannels[name];
  };
  dataChannelExist(name) {
    return this.yourDataChannels.hasOwnProperty(name);
  }
  get incomingDataChannels() {
    return Object.keys(this.yourDataChannels["incoming"]);
  }
  get outcomingDataChannels() {
    return Object.keys(this.yourDataChannels["outcoming"]);
  }

  async createAnswer(connection, offer) {
    await connection.setRemoteDescription(new RTCSessionDescription(offer));
    const answer = await connection.createAnswer();
    await connection.setLocalDescription(answer); // TODO: add error handlers
    return answer;
  }

  async createOffer(connection) {
    const offer = await connection.createOffer(DEFAULT_OFFER_OPTIONS);
    await connection.setLocalDescription(offer);
    return offer;
  }

  setupPeerConnection = destinationUserName => {
    const connection = new RTCPeerConnection(PEER_CONFIGURATION);
    this.addLocalAudioStream(connection);
    connection.ontrack = this.gotRemoteStream(destinationUserName);
    connection.onicecandidate = this.gotICECandidate(destinationUserName);
    connection.ondatachannel = this.gotDataChannel(destinationUserName);
    this.addNewConnection(destinationUserName, connection);
    this.createDataChannel(destinationUserName, connection);
    return connection;
  };

  createDataChannel = (name, connection) => {
    console.info("CREATING DATA CHANNEL for: " + name);
    const dataChannel = connection.createDataChannel(
      this.props.username,
      DATA_TRANSFER_OPTIONS
    );
    this.setupDataChannel(name, dataChannel, "outcoming");
  };

  setupDataChannel = (name, channel, group) => {
    channel.onmessage = this.handleReceiveMessage(name);
    channel.onopen = () => {
      console.info("OPENED " + group + " DATA CHANNEL WITH: " + name);
    };
    channel.onclose = this.handleReceiveChannelStatusChange(name, group); // FIXME: dosent fit with incoming and outcoming groups
    this.addNewDataChannel(name, channel, group);
  };

  gotDataChannel = name => ({ channel }) => {
    console.info("GOT DATA CHANNEL INCOMING: " + channel.label);
    this.setupDataChannel(channel.label, channel, "incoming");
    this.props.onNewPeerConnected(channel.label);
  };

  gotRemoteStream = name => ({ streams }) =>
    this.props.onNewStream(name, streams[0]);

  gotICECandidate = name => ({ candidate }) =>
    candidate && this.sendSignal({ type: "candidate", candidate, name });

  sendData = (name, data) => {
    const dataChannel = this.getDataChannel(name, "outcoming");
    if (dataChannel) dataChannel.send(JSON.stringify(data));
  };

  broadcastData = message => {
    console.info("broadcasting to: ", this.outcomingDataChannels.join(", "));
    this.outcomingDataChannels.map(channel => this.sendData(channel, message));
  };

  handleReceiveMessage = name => event => {
    const message = JSON.parse(event.data);
    console.info("-=== " + name + " : " + message + " ===-");
    this.props.onNewMessage(`${name}: ${message}`);
  };

  handleReceiveChannelStatusChange = (name, group) => () => {
    const channel = this.getDataChannel(name, group);
    if (channel)
      console.info(
        `${group} data channel ${name} status changed: ${channel.readyState}`
      );
  };

  startStreaming = () => {
    this.users.forEach(user =>
      this.addLocalVideoStream(this.yourConnections[user], user)
    );
  };

  stopStreaming = () => {
    this.users.forEach(user =>
      this.removeLocalVideoStream(this.getConnection(user), user)
    );
    this.sendSignal({ type: "hangup-video" });
  };

  addLocalVideoStream = async (connection, name) => {
    const localStream = this.props.stream;
    const videoTracks = localStream.getVideoTracks();
    const rtpSender = connection.addTrack(videoTracks[0], localStream);
    console.log(`rtpSender: ${rtpSender}`);
    this.senders[name] = rtpSender;
    console.group("--==> SENDERS ON ADD <==--");
    console.log("SENDERS: ", connection.getSenders());
    console.log("THIS.SENDERS: ", this.senders);
    console.groupEnd();
    try {
      const offer = await this.createOffer(connection, VIDEO_OFFER_OPTIONS);
      this.sendSignal({
        type: "offer",
        offer,
        name
      });
    } catch (e) {
      console.warn("Error while creating VIDEO stream offer: ", e);
    }
  };

  removeLocalVideoStream = async (connection, name) => {
    debugger;
    console.group("--==> SENDERS ON REMOVE <==--");
    console.log("SENDERS: ", connection.getSenders());
    console.log("THIS.SENDERS: ", this.senders);
    console.groupEnd();
    console.group("--==> RECEIVERS ON REMOVE <==--");
    console.log("RECEIVERS: ", connection.getReceivers());
    console.groupEnd();
    const rtpSender = this.senders[name];
    console.log("SENDER to delete: ", rtpSender);
    connection.removeTrack(rtpSender);
  };

  addLocalAudioStream = connection => {
    const localStream = this.props.stream;
    localStream
      .getAudioTracks()
      .forEach(track => connection.addTrack(track, localStream));
    console.log("Added local stream to your connection");
  };

  render() {
    const children = this.enchanceChildren();
    return <div>{children}</div>;
  }

  enchanceChildren = () => {
    return React.Children.map(this.props.children, child => {
      if (
        child.type.displayName &&
        child.type.displayName.match(/WebSocketConnection/)
      ) {
        return this.childWithHandlers(child);
      } else {
        return child;
      }
    });
  };

  childWithHandlers = child => {
    return React.cloneElement(child, {
      onWebSocketReady: this.handleWebSocketReady,
      onNewUser: this.onNewUser,
      onOffer: this.onOffer,
      onAnswer: this.onAnswer,
      onICECandidate: this.onICECandidate,
      onHangup: this.onHangup,
      onHangupVideo: this.onHangupVideo
    });
  };
}

export default RTCPConnection;
