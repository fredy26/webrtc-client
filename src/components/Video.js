// @flow
import React, { Component } from "react";

export default class Video extends Component {
  constructor(props) {
    super(props);
    this.video = React.createRef();
  }

  static defaultProps = {
    width: 250
  };

  componentDidMount() {
    const stream = this.props.stream;
    if (stream) {
      stream.oninactive = event =>
        console.log("Im INACTIVE: " + stream.name, event);
      stream.onactive = event =>
        console.log("Im ACTIVE: " + stream.name, event);
      stream.onaddtrack = event =>
        console.log("I have new TRACK: " + stream.name, event);
      stream.onremovetrack = event =>
        console.log("TRACK REMOVED: " + stream.name, event);
      this.loadVideo(this.video.current, stream);
    }
  }

  componentDidUpdate(prevProps) {
    const stream = this.props.stream;
    if (stream) {
      stream.oninactive = event =>
        console.log("Im INACTIVE U: " + stream.name, event);
      stream.onactive = event =>
        console.log("Im ACTIVE U: " + stream.name, event);
      stream.onaddtrack = event =>
        console.log("I have new TRACK U: " + stream.name, event);
      stream.onremovetrack = event =>
        console.log("TRACK REMOVED U: " + stream.name, event);
    }
    // if (this.props.stream !== prevProps.stream) {
    //   console.info("<Video> stream prop has changed, loading video");
    //   this.loadVideo(this.video.current, this.props.stream);
    // }
  }

  componentWillUnmount() {
    // this.detachStream();
  }

  detachStream = () => {
    debugger;
    const mediaStream = this.video.current && this.video.current.srcObject;
    if (!mediaStream) return;
    console.info("Detaching Stream");
    mediaStream.getTracks().forEach(track => {
      track.stop();
      mediaStream.removeTrack(track);
    });
    this.video.current.srcObject = null;
  };

  loadVideo = (video, stream) => {
    if (!video) console.warn("WTF!!!! Where is my video???");
    if (stream) {
      try {
        // video.srcObject = null; // eslint-disable-line no-param-reassign
        video.srcObject = stream; // eslint-disable-line no-param-reassign
      } catch (error) {
        console.warn("FUCK!!! Cant load Video", error);
      }
      video.play();
    }
  };

  // canPlay = width => event => {
  //   const video = event.currentTarget;
  //   if (!this.state.streaming) {
  //     // const { videoHeight, videoWidth } = video;
  //     // const height = videoHeight / (videoWidth / width);
  //     video.setAttribute("width", width);
  //     video.setAttribute("height", width);
  //     this.setState({ streaming: true });
  //   }
  // };

  render() {
    return (
      <>
        <video
          autoPlay
          ref={this.video}
          className={`${this.props.className || ""}`}
        >
          Video stream not available.
        </video>
      </>
    );
  }
}
