const styles = {
  "@global": {
    body: {
      backgroundColor: "white"
    }
  },
  paper: {
    marginTop: "50px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: "15px",
    backgroundColor: "white",
    color: "black"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: "15px"
  },
  submit: {
    marginTop: "20px"
  }
};

export default styles;
