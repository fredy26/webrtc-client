export function hasUserMedia() {
  return navigator.mediaDevices && navigator.mediaDevices.getUserMedia;
}

export async function setupLocalStream() {
  const constraints = { video: true, audio: false };
  try {
    const localStream = await navigator.mediaDevices.getUserMedia(constraints);
    return localStream;
  } catch (err) {
    console.warn("Problem while getting local stream: ", err);
  }
}
