import React from "react";
import Chip from "@material-ui/core/Chip";
import Paper from "@material-ui/core/Paper";
import TagFacesIcon from "@material-ui/icons/TagFaces";

const styles = {
  chip: { margin: "10px" },
  paper: {
    display: "flex",
    justifyContent: "start",
    flexWrap: "wrap",
    padding: "10px"
  },
  title: { fontWeight: "600", float: "left" }
};

const UserChip = props => (
  <Chip
    icon={<TagFacesIcon />}
    color="secondary"
    label={props.name}
    style={styles.chip}
  />
);

const UsersOnline = ({ users }) => {
  const list = users.map(name => <UserChip key={name} name={name} />);
  return (
    <>
      <Paper style={styles.paper}>
        <span style={styles.title}>Users online:</span>
        {list}
      </Paper>
    </>
  );
};

export default UsersOnline;
