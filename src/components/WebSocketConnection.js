import React, { Component } from "react";
import { HOST } from "./config";

class WebSocketConnection extends Component {
  constructor(props) {
    super(props);
    this.webSocketConnection = new WebSocket(HOST); // TODO: should be kept in global state
    this.webSocketConnection.onopen = this.onOpen;
    this.webSocketConnection.onerror = this.onError;
    this.webSocketConnection.onmessage = this.dispatchMessage;
  }

  static displayName = "WebSocketConnection";

  componentDidMount() {
    this.props.onWebSocketReady(this.sendSignal);
  }
  componentWillUnmount() {
    this.webSocketConnection.close();
  }

  onOpen = () => console.info("Connected to: " + HOST);
  onError = error => console.warn("Connection error: ", error);

  dispatchMessage = message => {
    const data = JSON.parse(message.data);
    const {
      onLogin,
      onJoin,
      onNewUser,
      onOffer,
      onAnswer,
      onICECandidate,
      onHangup,
      onHangupVideo
    } = this.props;
    switch (data.type) {
      case "login":
        onLogin(data);
        break;
      case "join":
        onJoin(data);
        break;
      case "new-user":
        onNewUser(data);
        break;
      case "offer":
        onOffer(data.offer, data.from);
        break;
      case "answer":
        onAnswer(data.answer, data.from);
        break;
      case "candidate":
        onICECandidate(data.candidate, data.from);
        break;
      case "hangup":
        onHangup(data);
        break;
      case "hangup-video":
        onHangupVideo(data);
        break;
      default:
    }
  };

  sendSignal = message => {
    this.webSocketConnection.send(JSON.stringify(message));
  };
  // TODO: this should be a hook for cleaner implementation
  render() {
    const withWebSocket = React.Children.map(this.props.children, child =>
      React.cloneElement(child, {
        sendSignal: this.sendSignal,
        webSocketConnection: this.webSocketConnection
      })
    );
    return <div>{withWebSocket}</div>;
  }
}

export default WebSocketConnection;
