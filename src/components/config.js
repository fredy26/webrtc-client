/* eslint-disable no-restricted-globals */
export const PEER_CONFIGURATION = {
  iceServers: [
    {
      urls: "turn:numb.viagenie.ca",
      username: "fredy26@mail.ru",
      credential: "laskala"
    },
    {
      urls: ["stun:64.233.165.127:19302", "stun:[2a00:1450:4010:c08::7f]:19302"]
    },
    {
      urls: [
        "turn:64.233.161.127:19305?transport=udp",
        "turn:[2a00:1450:4010:c01::7f]:19305?transport=udp",
        "turn:64.233.161.127:19305?transport=tcp",
        "turn:[2a00:1450:4010:c01::7f]:19305?transport=tcp"
      ],
      username: "CNHB3ugFEgaETAqSFDAYqvGggqMKIICjBQ",
      credential: "Upp7XgFKLzBd6fYO/0TXhnxMF1Y=",
      maxRateKbps: "8000"
    }
  ]
};

export const HOST = "wss://basic-signal-server.herokuapp.com"; // location.origin.replace(/^http/, "ws");
// export const HOST = "ws://localhost:8888"; //location.origin.replace(/^http/, "ws");

export const DATA_TRANSFER_OPTIONS = { ordered: true };
