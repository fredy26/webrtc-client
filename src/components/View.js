import React, { Component } from "react";
import { Widget, addResponseMessage } from "react-chat-widget";
import Switch from "@material-ui/core/Switch";

import WebSocketConnection from "./WebSocketConnection";
import RTCPConnection from "./RTCPConnection";
// import Videos from "./Videos";
import Video from "./Video";
import Login from "./Login";
import { hasUserMedia, setupLocalStream } from "./utils";

import * as stateActions from "./stateActions";

import "react-chat-widget/lib/styles.css";
import UsersOnline from "./UsersOnline";

class View extends Component {
  state = {
    rooms: null,
    streaming: false,
    streams: {},
    dataChannels: [],
    onlineUsers: [],
    loggedIn: false,
    joined: false,
    localStream: null
  };

  componentDidMount() {
    this.initCamera();
  }

  loggedIn = ({ success, rooms, error }) => {
    if (success) {
      console.info("User loggeed In. We have rooms: " + rooms);
      this.setState({ loggedIn: success, rooms });
    } else {
      alert("Error while login: " + error);
    }
  }; // show me the rooms list

  joined = ({ success, error }) => {
    if (success) {
      this.setState({ joined: success });
    } else {
      alert("Attempt to join the room was unsuccessful. " + error);
    }
  };

  // TODO: remove unused UI elements
  removePeer = name => {
    this.removeStream(name);
    this.removePeerFromList(name);
  };
  removeStream = name => {
    console.log("User " + name + " disconnected. Removing stream");
    this.setState(stateActions.removeStream(name));
  };
  addStream = (name, stream) => {
    stream.name = name;
    this.setState(stateActions.addStream(name, stream));
  };

  addPeerToList = name =>
    this.setState({ onlineUsers: this.state.onlineUsers.concat(name) });

  removePeerFromList = name =>
    this.setState({
      onlineUsers: this.state.onlineUsers.filter(user => user !== name)
    });

  initCamera = async () => {
    if (hasUserMedia()) {
      const localStream = await setupLocalStream();
      this.setState({ localStream });
    } else {
      console.warn("Nope, no WebRTC for this browser");
    }
  };

  setUserName = name => {
    this.setState({ username: name });
    this.setState(state => {
      const localStream = state.localStream;
      localStream.name = name;
      return { localStream };
    });
  };

  handleNewMesage = message => addResponseMessage(message);

  handleNewUserMessage = message => {
    console.info(message);
    this.broadcastMessage(`${message}`);
  };

  handleSubscribe = ({ transport, startStreaming, stopStreaming }) => {
    this.broadcastMessage = transport;
    this.startStreaming = startStreaming;
    this.stopStreaming = stopStreaming;
  };

  handleSwitchVideo = event => {
    this.setState({ streaming: event.target.checked }, () => {
      if (this.state.streaming) {
        console.info("STREAMING ON");
        this.startStreaming();
      } else {
        console.info("STREAMING OFF");
        this.stopStreaming();
      }
    });
  };

  render() {
    return (
      <>
        <UsersOnline users={this.state.onlineUsers} />
        <RTCPConnection
          username={this.state.username}
          stream={this.state.localStream}
          onNewStream={this.addStream}
          onNewMessage={this.handleNewMesage}
          onSubscribe={this.handleSubscribe}
          onNewPeerConnected={this.addPeerToList}
          onHangup={this.removePeer}
          onHangupVideo={this.removeStream}
        >
          <WebSocketConnection onLogin={this.loggedIn} onJoin={this.joined}>
            {this.state.joined ? (
              <>
                Start Streaming:
                <Switch
                  checked={this.state.streaming}
                  onChange={this.handleSwitchVideo}
                />
                <Video
                  key={this.state.localStream.id}
                  stream={this.state.localStream}
                />
                {Object.values(this.state.streams).map(stream => (
                  <Video key={stream.id} stream={stream} />
                ))}
                {/* <Videos
                  localStream={this.state.localStream}
                  streams={this.state.streams}
                /> */}
              </>
            ) : (
              <Login
                rooms={this.state.rooms}
                show={!this.state.joined}
                onUserName={this.setUserName}
              />
            )}
          </WebSocketConnection>
          <Widget handleNewUserMessage={this.handleNewUserMessage} />
        </RTCPConnection>
      </>
    );
  }
}

export default View;
