import React from "react";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";

import Video from "./Video";

const Videos = ({ localStream, streams }) => {
  const renderVideo = stream =>
    stream ? (
      <GridListTile key={stream.id} cols={1}>
        <Video key={stream.id} stream={stream} />
        <GridListTileBar
          style={{
            height: "20px",
            width: "250px",
            margin: "auto",
            lineHeight: "1.2"
          }}
          subtitle={<span style={{ fontSize: "15px" }}>{stream.name}</span>}
        />
      </GridListTile>
    ) : null;

  return (
    <div
      style={{
        marginTop: "45px",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-around",
        overflow: "hidden"
      }}
    >
      <GridList style={{ width: "620px" }} cols={2}>
        {localStream && renderVideo(localStream)}
        {streams && Object.values(streams).map(stream => renderVideo(stream))}
      </GridList>
    </div>
  );
};

export default Videos;
